import { Router } from "../../utils/common.js";
import { RemoteDataService } from '../../utils/remoteDataService.js';

Page({
  data: {
    petList: [],
    userId: "",
    userNickName: ""
  },
  onLoad: function (option) {
    let that = this
    if (option && option.userId){
      that.data.userId = option.userId
    }
  },
  onShow: function () {
    this.getSomeonePetList()
  },
  getSomeonePetList: function () {
    let that = this
    let params = {
      userId: that.data.userId
    };
    wx.showNavigationBarLoading()
    RemoteDataService.getSomeonePetList(params).then(result => {
      if (result && result.code == "000") {
        console.log(result.pets)
        that.setData({
          petList: result.pets,
          userNickName: result.userNickName
        })
      }
      wx.hideNavigationBarLoading()
    }).catch(err => {
      wx.hideNavigationBarLoading()
    })
  },
  navToPetDet: function (e) {
    let params = {
      petId: e.currentTarget.dataset.petid
    }
    Router.navigateTo("../dogdet/dogdet", params);
  }
})
